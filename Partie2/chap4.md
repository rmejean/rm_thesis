
\lettrine{N}{ous présentons} dans ce chapitre une première approche de modélisation et de simulation des dynamiques de déforestation de notre territoire d'étude en Amazonie équatorienne, la *parroquia* de Dayuma. Il s'agit d'un modèle de changements d'occupation et d'usage du sol de type "*pattern-based*" (PBM) développé avec le Land change modeler (LCM) de TerrSet. Les principes de fonctionnement des PBM ont été abordés dans le chapitre 2. Pour rappel, ce type de modèle repose sur les relations entre l'analyse des changements d'occupation et d'usage du sol passés et un ensemble de variables spatiales explicatives préalablement identifiées — aussi appelées *"drivers"*. Comme nous l'avons évoqué dans la première partie de cette thèse et plus particulièrement dans le chapitre précédent, la déforestation de l'Amazonie équatorienne est en effet un phénomène alimenté par des processus multiscalaires (marchés internationaux, migrations internes, décisions locales), des acteurs variés (institutionnels, individuels) et dépend également d'un contexte physique, historique, politique, environnemental et social particulier, approché dans le modèle par cet ensemble de variables explicatives.

Afin de satisfaire les objectifs de notre démarche comparative entre approches de modélisation des changements d'occupation et d'usage du sol, mais également pour des raisons de disponibilité des données, nous avons fait le choix de réaliser un modèle simple, avec peu de catégories d’occupation du sol et une seule transition à simuler : la déforestation\footnote{Soit la transition de la classe "forêt" à la classe "agriculture".}. Pour les raisons expliquées à la fin du chapitre 2 (cf. section \ref{subsec:chap2-3-7}), le modèle se devait de simuler des données passées. C'est pourquoi celui-ci est calibré sur la période 2000-2008 (soit de *t0* à *t1*) et simule des changements pour 2016 (*t2*).

Après avoir présenté les données d'occupation et d'usage du sol sélectionnées pour le modèle puis la collecte, le traitement et la sélection des variables utilisées pour sa calibration, nous détaillerons le fonctionnement de notre modèle PBM et présenterons les résultats de simulation ainsi que des éléments d'évaluation/validation.

\section{Données}
\label{sec:chap4-1}

\subsection{Données d'occupation et d'usage du sol}
\label{subsec:chap4-1-1}

Les modèles de changements d'occupation et d'usage du sol de type PBM sont basés sur les changements passés et leurs relations avec un ensemble de variables explicatives.

Il est donc nécessaire d'analyser au préalable les dynamiques spatio-temporelles de l'occupation et de l'usage du sol, ce qui a été fait et présenté dans le chapitre 3 à partir des classifications d'images satellite produites par les pouvoirs publics équatoriens (cf.~section \ref{subsec:chap3-3-2}). Les données utilisées pour ces analyses sont donc aussi celles qui ont servi de support à l'élaboration du modèle PBM, à savoir : les classifications de l'occupation et de l'usage du sol de la région de Dayuma pour les années 2000 (*t0*), 2008 (*t1*) et 2016 (*t2*), en quatre catégories : les eaux de surface, les espaces forestiers, agricoles et urbains (Figure~\ref{fig:fig1}). Les deux premières classifications définissent les bornes temporelles de la période de calibration du modèle, établie sur une période de huit années (2000-2008). L'intervalle temporel est d'une durée identique jusqu'à la dernière classification (2016), qui fixe l'horizon de la période de simulation du modèle (2008-2016).

\begin{figure}[!h]\centering
\includegraphics[width=\linewidth]{Partie2/figures/classifs.png}
\caption{Classifications de l'occupation et de l'usage du sol utilisées pour le modèle PBM.}\label{fig:fig1}
\end{figure}

\subsection{Variables explicatives}
\label{subsec:chap4-1-2}

\subsubsection{Identification des facteurs et données disponibles}
\label{subsubsec:chap4-1-2-1}


Le choix des facteurs explicatifs à utiliser pour calibrer un modèle PBM est basé sur une connaissance des dynamiques à l’œuvre tirée de références bibliographiques, d'enquêtes de terrain ou encore de dires d'experts et dépend des données spatiales disponibles. Pour entraîner le modèle, il convient en effet de choisir des variables corrélées à l'apparition des changements d'occupation et d'usage du sol. Comme nous l'avons vu dans le chapitre 3 (cf.~\ref{sec:chap3-2}), l'Amazonie équatorienne connaît des dynamiques de front pionnier : une conversion des sols caractérisée par une déforestation au profit des activités agricoles, entraînée par une dynamique de migration interne amorcée et alimentée par les activités d'extraction pétrolière.

Une première étape a consisté à collecter des données nous permettant de générer les facteurs de changements traditionnellement utilisés dans les modèles de type PBM --- la plupart sont relatifs au site et à la situation géographique --- déjà éprouvés par des démarches de modélisation antérieures [@mas2018] : la distance au réseau routier [@rudel1983], la distance à la déforestation déjà existante, les caractéristiques topographiques (MNT) et pédologiques du terrain d'étude. Les données permettant de calculer ces variables spatiales ont pu être collectées facilement auprès du programme SIGTIERRAS\\footnote{*"Sistema Nacional de Información de Tierras Rurales e Infraestructura Tecnológica"*, programme mis en place par le ministère de l'agriculture équatorien — le MAG, *Ministerio de Agricultura y Ganadería* — ayant pour but d'améliorer la gestion administrative du territoire national, en réalisant notamment les levés cadastraux de 57 cantons du pays.}, il s'agit notamment d'un MNT d'une résolution spatiale de cinquante mètres, du réseau routier vectorisé à l'échelle nationale ainsi que d'une couche de données géographiques concernant la géo-pédologie.

Si ces facteurs explicatifs sont particulièrement récurrents dans les modèles de déforestation en contextes de fronts pionniers [@mas2004 ; @islam2018], c'est parce qu'ils synthétisent le modèle théorique de la déforestation tropicale tel que décrit dans la littérature scientifique [@allen1985 ; @laurance2002 ; @arima2013 ; @fearnside2015], à savoir : une déforestation qui se produit à proximité des axes routiers, de manière contiguë à la déforestation déjà existante et sur les espaces les plus propices à la conversion vers des sols agricoles (évitant les fortes pentes et privilégiant les sols les plus fertiles).

Ensuite, afin d'ajouter une dimension thématique et contextuelle spécifique aux dynamiques de déforestation de l'Amazonie équatorienne, il était nécessaire d'intégrer au modèle les facteurs explicatifs d'ordre socio-démographique qui jouent un rôle important dans la transformation du territoire, comme nous l'avons évoqué dans le chapitre précédent [@sellers2017 ; @jarrin-v.2017]. Les données utilisées sont celles du recensement de la population équatorien de 2010, réalisé par l'INEC\\footnote{*Instituto Nacional de Estadística y Censos*, équivalent de l'INSEE français.}. Elles concernent à la fois la répartition spatiale de la population dans le territoire, la structure des ménages, la taille et le niveau d'équipement de leur habitation (*vivienda*) ou encore le lieu de naissance des individus. En permettant d'identifier et de distinguer les individus nés dans la Sierra de ceux nés en Amazonie équatorienne, ces données sont particulièrement utiles pour prendre en compte le facteur explicatif de la migration interne et le contexte historique [@southgate1991 ; @brown1992 ; @sierra2000].

Enfin, toujours dans l'idée d'apporter des éléments contextuels spécifiques à notre terrain d'étude, il apparaissait évident de prendre en compte les activités d'extraction pétrolière dans notre démarche de modélisation, puisqu'il s'agit d'un facteur explicatif des changements d'occupation et d'usage du sol de la région. Les données à notre disposition sont des informations spatiales établies par le PRAS\\footnote{Le *Programa de Reparacion Ambiental y Social*, créé en 2008, étudie les impacts socio-environnementaux des activités économiques, notamment l'extraction d'hydrocarbures.} et concernent la localisation de sites et d'équipements liés à l'industrie pétrolière : emplacement des torchères (*mecheros*), des puits (*pozos*), des fosses (*fosas*), etc.


\input{Partie2/figures/tables/tab1}


Nous pouvons ainsi regrouper les différents facteurs de changement identifiés en trois catégories (voir Tableau~\ref{tab:tab1}) : les facteurs relevant du site et de la situation géographiques (MNT, déforestation à *t0*, réseau routier)~~;~~les facteurs d'ordre socio-démographique (localisation, structure et niveau d'équipement des ménages)~~;~~les activités d'extraction pétrolière.

La conversion de ces facteurs de changements en variables explicatives à intégrer au modèle PBM passe par des traitements de données visant leur spatialisation. Ces étapes sont explicitées dans la sous-section suivante. 

\subsubsection{Spatialisation et sélection des facteurs}
\label{subsubsec:chap4-1-2-2}

Les variables explicatives à implémenter dans un PBM doivent être de nature spatiale, afin de pouvoir être utilisées pour l'apprentissage du modèle et générer les cartes de probabilité de transition qui servent de base à l'allocation spatiale des quantités de changements prévues par les chaînes de Markov. Cette section détaille les manipulations géomatiques employées pour spatialiser chacun des facteurs identifiés. 


\textbf{Facteurs liés au site et à la situation géographique}
\label{subsubsubsec:chap4-1-2-2-1}

Le réseau routier issu des données SIGTIERRAS a été intégré au modèle grâce à sa transformation en variable spatiale continue. Pour cela, la distance euclidienne ("à vol d'oiseau") entre chaque pixel du territoire et le réseau routier est calculée (Figure~\ref{fig:fig2}, a, \textsc{DisRoute}).

La même transformation est opérée pour intégrer au modèle la déforestation existante à *t0*, soit en 2000. La date la plus ancienne est choisie pour formaliser l'hypothèse que les changements survenus ont été affectés par la configuration du paysage au début de la période [@mas2018]. La distance euclidienne entre chaque pixel et la déforestation en 2000 est calculée (Figure~\ref{fig:fig2}, b, \textsc{DisDef}). Cette variable spatiale sera définie comme "dynamique" au sein de l'environnement de LCM, ce qui signifie qu'elle sera recalculée au cours de chaque itération de la simulation, un pas de temps annuel. 

Nous avons également décidé d'intégrer au modèle la distance à la déforestation survenue entre les années 1990 et 2000, afin d'ajouter la logique spatiale de défrichement entre deux dates suffisamment éloignées (figure~\ref{fig:fig2}, c, \textsc{DisDef2}). 

Enfin, le modèle numérique de terrain (MNT) utilisé, d'une résolution spatiale de 50 mètres, provient également des données du programme SIGTIERRAS (figure~\ref{fig:fig2}, d, \textsc{MNT}). Il n'a fait l'objet d'aucune transformation ni normalisation.

\begin{figure}[!h]\centering
\includegraphics[width=\linewidth]{Partie2/figures/fateurs_2.png}
\caption[Premier ensemble de variables : distances euclidiennes et MNT]{Distances euclidiennes (a) au réseau routier~~;~~(b) à la déforestation en 2000~~;~~(c) à la déforestation survenue entre 1990 et 2000~~;~~(d) modèle numérique de terrain. (MNT)}\label{fig:fig2}
\end{figure}


Le coefficient V de Cramer [@cramer1991, chap. 21] a été calculé pour évaluer la relation entre chacune de ces variables spatiales et la transition de la classe "forêt" à la classe "agriculture". Ce coefficient, qui oscille entre 0 et 1, est d'autant plus élevé que la corrélation est forte et vice versa. Il est recommandé d'écarter les variables dont le V est inférieur à 0.15, car jugées peu suffisamment explicatives pour entraîner le modèle \\citep\[p. 209\]{maestripieri2012}. Nous avons fait le choix de sélectionner et d'intégrer au modèle uniquement les variables dont V~>~0.2. Ainsi, le MNT n'a pas été retenu (voir Tableau~\ref{tab:tab2})

\input{Partie2/figures/tables/tab2}

\textbf{Facteurs socio-démographiques}
\label{subsubsubsec:chap4-1-2-2-2}


Des variables explicatives socio-démographiques ont pu être utilisées pour l'entraînement du modèle. La donnée utilisée est issue du recensement national de la population effectué en 2010 et correspond à ce que, en France, l'INSEE appelle le "fichier détail" du recensement. Il s'agit d'une base de données volumineuse dans laquelle chaque ligne correspond à un individu (ou à un ménage). 

Dans le système censitaire équatorien, chacun des individus statistiques de ce fichier est rattaché à une localité rurale (*localidades dispersas*), qui appartient elle-même à un secteur, compris dans une zone. C'est la même chose pour les espaces urbanisés, où chaque individu est rattaché à un îlot (*manzana*), qui appartient à un secteur, rattaché à une zone. *Localidades dispersas* et *manzanas* constituent ainsi les unités censitaires les plus fines du recensement national équatorien. 

Ainsi, après avoir récupéré la couche de points de l'ensemble des localités rurales de notre zone d'étude et après avoir calculé le centroïde des *manzanas* pour convertir les polygones des îlots en couche ponctuelle, nous avons pu filtrer le fichier national pour en extraire uniquement les informations relatives aux individus vivant sur notre territoire d'étude, la *parroquia* de Dayuma\\footnote{Pour filtrer le fichier national à l'aide des identifiants de chacune des localités, nous avons utilisé conjointement les logiciels SPSS et Redatam. Ce dernier est développé par la CEPALC (Commission économique pour l'Amérique latine et les Caraïbes), organe des Nations Unies.}. À partir de ces informations, nous avons pu spatialiser les variables statistiques qui nous intéressaient par une interpolation de type TIN [@floriani2009]. Pour éviter un effet de bord, les points compris dans une zone tampon de dix kilomètres au-delà de la zone d'étude ont été inclus dans les traitements (voir figure~\ref{fig:fig3}).

\begin{figure}[h]\centering
\includegraphics[width=\linewidth]{Partie2/figures/points2.png}
\caption{Localités rurales et *manzanas* utilisées pour spatialiser les variables statistiques.}\label{fig:fig3}
\end{figure}


Grâce à ces manipulations, des informations recueillies par le recensement telles que la structure par âge de la population, la structure des ménages et leur niveau d'équipement, etc., sont spatialisées. Les variables statistiques à "transformer en pixels" (US~\citealp{usnrc1998}) ont été choisies à partir des connaissances théoriques et empiriques évoquées dans le chapitre~3, notamment leur rôle incitatif démontré ou supposé dans les dynamiques de déforestation locales : la présence de jeunes enfants au sein des ménages, la taille des familles, leur province de naissance (pour identifier les colons), etc. Une donnée supplémentaire a été ajoutée au corpus de variables explicatives : la distance euclidienne aux puits de pétrole, calculée à partir d'une couche de points inventoriant l'ensemble de ces infrastructures ayant joué un rôle structurant en Amazonie équatorienne. Après avoir une nouvelle fois calculé le coefficient V de Cramer (Tableau~\ref{tab:tab3}) pour évaluer la relation de chacune de ces variables avec la transition de la classe "forêt" à la classe "agriculture", nous avons choisi d'intégrer dans notre modèle PBM les variables suivantes :
\newline
\newline

* \textsc{Pop} : localisation\footnote{La répartition spatiale de chacune des populations de cette liste est estimée par une interpolation spatiale de type TIN calculée à partir des unités censitaires ponctuelles de l'aire d'étude et de l'aire d'étude étendue (voir figure~\ref{fig:fig3}).} de la population
* \textsc{Pop\char`_0-14} : localisation de la population âgée de moins de 14 ans
* \textsc{Pop\char`_65+} :  localisation de la population âgée de plus de 65 ans
* \textsc{Pop\char`_Sierra} : localisation des individus nés dans une province de la Sierra
* \textsc{Pop\char`_Oriente} : localisation des individus nés dans une province de l'Oriente
* \textsc{Fam+} : localisation des grandes familles (six enfants et plus)
* \textsc{Viv\char`_1} : localisation des petites *viviendas* (une ou deux chambres)
* \textsc{Viv\char`_2} : localisation des moyennes *viviendas* (trois ou quatre pièces)
* \textsc{Viv\char`_3} :  localisation des grandes *viviendas* (six pièces et plus)
* \textsc{Viv\char`_Elec} : localisation des *viviendas* raccordées au réseau électrique
* \textsc{Viv\char`_San} : localisation des *viviendas* équipées de sanitaires
* \textsc{Dis\char`_Pozos} : distance aux puits de pétrole


\input{Partie2/figures/tables/tab3}


\begin{figure}[p]\centering
\includegraphics[width=\linewidth]{Partie2/figures/regroupement_variables2.png}
\caption[Deuxième ensemble : les variables socio-démographiques]{Variables statistiques socio-démogaphiques spatialisées. La numérotation des variables est indiquée dans le tableau \ref{tab:tab3}.}\label{fig:fig4}
\end{figure}


\section{Le modèle}
\label{sec:chap4-2}

Notre modèle PBM a été développé avec le module Land Change Modeler (LCM) de TerrSet [@eastman2014 ; @eastman2018]. L'objectif de ce modèle est de simuler les changements d'occupation et d'usage du sol survenus pendant la période 2008-2016 à partir d'une calibration réalisée sur la période 2000-2008. Le choix des périodes de calibration et de simulation, crucial [@paegelow2018], a été contraint par la disponibilités des données, puisque nous disposions de classifications de l'occupation et de l'usage du sol pour ces trois dates. Il s'agit ainsi de la simulation d'un scénario prospectif dit *"business-as-usual"*, consistant à prolonger les changements observés dans le passé (2000-2008) à partir de leurs relations avec les variables explicatives sélectionnées précédemment. Le modèle est donc calibré sur une période de huit années pour simuler des changements survenant huit années plus tard (Figure~\ref{fig:fig5}).

\begin{figure}[b]\centering
\includegraphics[width=\linewidth]{Partie2/figures/modele.png}
\caption{Périodes de calibration et de simulation du modèle.}\label{fig:fig5}
\end{figure}

LCM utilise, entre autres options disponibles, un réseau neuronal artificiel de type perceptron multi-couches\\footnote{En anglais, *multi-layer perceptron*, MLP.} comme celui de la figure~\ref{fig:MLP} [@murtagh1991 ; @meyer-baese2014 ; @taud2018] pour produire des cartes de probabilité de transition (appelées *"soft predictions"*) à l'issue de la calibration du modèle. Ces cartes exprimant une probabilité d’occurrence de changements d'occupation du sol sont ensuite utilisées par une procédure de *competitive land allocation* pour allouer spatialement les quantités de changements futurs estimées par des chaînes de Markov et produire des projections pour l'année 2016 (*"hard predictions"*). La Figure~\ref{fig:fig6} illustre notre démarche de modélisation *pattern-based* sous LCM.

\begin{figure}[h]\centering
\includegraphics[width=.7\linewidth]{Partie2/figures/MLP_parizeau.png}
\caption[Exemple d'un réseau neuronal de type MLP]{Exemple d'un réseau neuronal de type MLP. Figure extraite de @parizeau2004.}\label{fig:MLP}
\end{figure}


\subsection{Les transitions}
\label{subsec:chap4-2-1}

L'analyse des changements passés survenus au cours de la période 2000-2008 souligne trois transitions majeures (cf. chapitre 3) : (1) de la classe "forêt" vers la classe "agriculture", qui correspond aux dynamiques de déforestation~~;~~(2) de la classe "agriculture" vers la classe "forêt", qui concerne l'abandon de terres agricoles, donc l'enfrichement et enfin (3) de la classe "forêt" vers la classe "espace urbanisé", qui correspond quant à elle à l'étalement des villes et villages de la zone d'étude.

Pour plusieurs raisons, nous avons fait le choix de ne simuler que les dynamiques de déforestation (qui correspondent aux *"Changements t0 - t1"*, sur la figure~\ref{fig:fig6}). Il nous paraissait d'abord important de conserver un modèle simple afin de permettre la comparaison entre approches de modélisation qui fait l'objet de cette thèse. Ensuite, parce que les variables explicatives utilisées pour entraîner le modèle ont été sélectionnées en fonction de leur lien avec la déforestation. Enfin, parce que les deux autres transitions, qui correspondent respectivement aux dynamiques d'enfrichement et d'étalement urbain en contexte de front pionnier, sont des objets d'étude à part entière, différents de celui qui nous préoccupe et nécessiteraient, pour être simulées, la sélection puis l'intégration de variables spatialisées particulières.


Comme nous le verrons, ce choix de ne simuler qu'une seule des transitions survenues pendant la période de calibration n'est pas exempt de conséquences sur les performances et les résultats du modèle, indépendamment des particularités du territoire de l'Amazonie équatorienne.

\subsection{Potentiel de transition}
\label{subsec:chap4-2-2}

Pour chaque transition présente dans le sous-modèle, le réseau neuronal artificiel de LCM produit des cartes du potentiel de transition à partir des changements observés sur la période de calibration et des variables explicatives (*drivers*). À l'issue de ces opérations et de la simulation, LCM génère une carte générale du potentiel de transition. En raison de son caractère continu, cette carte est appelée *"soft prediction"* par LCM, une appellation qui peut également désigner les cartes de *"change potential"*, de *"transition probability"*, de *"susceptibility to land change"*, de *"propensity to change"*, ou encore *"suitability"* qui sont produites par d'autres logiciels de modélisation et que l'on retrouve dans la littérature. Comme l'ont montré @camachoolmedo2018d dans une entreprise de clarification du jargon, l'existence de ces différents termes est due à des différences méthodologiques entre les processus de modélisation des différents logiciels. Sans entrer dans les détails, la carte calculée par LCM est une *transition potential map*, qui appartient aux *intermediate soft-classified maps* regroupant les cartes dénuées de référence temporelle future\\footnote{Ici, l'information relative aux quantités de changements futurs est distincte, apportée par les matrices de Markov.}. Ainsi, comme le rappelle \\citet\[p. 235\]{maestripieri2012}, il ne s'agit pas d'une carte des pixels qui vont changer mais plutôt d'une carte classant (*ranking*) les pixels selon leur potentiel de transition.
								
						

																																																																																																																																																																																																																																																																																																																																																																																																			   


\begin{landscape}
\begin{figure}[p]\centering
\includegraphics[width=\linewidth]{Partie2/figures/resume_modele.png}
\caption[Démarche de modélisation PBM sous Land Change Modeler]{Démarche de modélisation PBM sous Land Change Modeler, illustrée par notre modèle.}\label{fig:fig6}
\end{figure}
\end{landscape}



La carte (figure~\ref{fig:fig7}), produite à l'issue de la calibration de notre modèle, indique donc le potentiel de transition de la classe "forêt" vers la classe "agriculture" de chaque pixel, sur une échelle de 0 à 1. Compte tenu des variables explicatives, sans surprise, les pixels ayant la probabilité de transition la plus importante se situent à proximité directe de la déforestation qui existait à *t0*, en 2000, des routes et des principaux foyers de population. À l'inverse, les espaces aux probabilités de transition les plus faibles se situent en marge de la *vía Auca* et de la traditionnelle structure spatiale de déforestation en forme d’arêtes de poisson (cf. section~\ref{subsec:chap3-2-2}), ce qui correspond peu ou prou aux territoires alloués aux populations indigènes (*comunas*) et au parc Yasuní, situé à l'est de la zone d'étude.


\begin{figure}[!h]\centering
\includegraphics[width=\linewidth]{Partie2/figures/transition_potential3.png}}
\caption[Carte du potentiel de transition]{Carte du potentiel de transition. Les pixels les plus clairs sont ceux où le potentiel de transition est le plus important.}\label{fig:fig7}
\end{figure}



\subsection{Allocation spatiale des changements}
\label{subsec:chap4-2-3}

D'après @camachoolmedo2018d, en modélisation PBM, la procédure d'allocation spatiale des changements est "un processus de décision qui sélectionne à partir de [la carte du potentiel de transition] les pixels les plus susceptibles de passer d'une classe à une autre". Dans LCM, ce processus de décision est un algorithme appelé *Multi-Objective Land Allocation* (MOLA), qui traite la compétition et les conflits entre les différentes transitions possibles pour un même pixel par une procédure d'optimisation itérative décrite par @eastman1995.

Dans notre modèle, l'allocation spatiale des quantités de changements de la classe "forêt" vers la classe "agriculture" prévues par les matrices de Markov (tableau~\ref{tab:tab4}) est donc basée sur la carte figure~\ref{fig:fig7}. Rappelons que les matrices de Markov sont calculées à partir des changements survenus durant la période 2000-2008, dite période d'entraînement du modèle. Celles-ci reposent donc sur l'hypothèse d'une dynamique temporelle linéaire, autrement dit sur la projection dans le futur des dynamiques passées, toutes choses égales par ailleurs, bien que la plupart des logiciels permettent de les éditer manuellement pour l'élaboration de scénarios prospectifs contrastés.


L'allocation spatiale des changements aboutit à la production d'une *hard prediction*, une carte à interpréter en tant que sortie du modèle, résultat de la simulation, où chaque pixel est classifié dans une des catégories d'occupation du sol. La sous-section suivante présente, commente et analyse ces résultats au moyen de plusieurs méthodes de validation.

\input{Partie2/figures/tables/tab4}


\section{Résultats et validation}
\label{sec:chap4-2-3}

Compte tenu du caractère "rétrospectif" des simulations, il s'agit ici d'évaluer la capacité du modèle à reproduire des données existantes, ce que \citet{graebner2018} nomme "validation des résultats descriptifs" (cf. section \ref{subsec:chap2-2-3-2-3}).

\begin{figure}[!h]\centering
\includegraphics[width=\linewidth]{Partie2/figures/comparaison_visuelle.png}
\caption[Comparaison visuelle entre la classification et le résultat de la simulation]{Comparaison visuelle entre la classification MAE-MAGAP pour 2016 (à gauche) et le résultat de la simulation pour la même année (à droite).}\label{fig:fig8}
\end{figure}

Une comparaison visuelle entre la classification de l'occupation du sol observée en 2016 (*t2*) et la carte *hard prediction* simulée par le modèle pour la même année (*t2\textunderscore sim*) permet une première appréciation de la qualité de la simulation (Figure~\ref{fig:fig8}). Il semblerait que le modèle surestime la déforestation survenue entre 2008 et 2016, ce qui est confirmé lors de l'examen du Tableau~\ref{tab:tab5} qui compare la part du territoire occupée par chacune des catégories pour la carte 2016 observé et 2016 simulé. En effet, alors que les espaces forestiers occupent 83.1\% de la zone d'étude en 2016 sur la classification définie comme la "situation observée" (*t2*), ils ne représentent que 81.8\% de la surface du territoire sur la sortie du modèle, la "situation simulée" (*t2\textunderscore sim*). Cette surestimation de près de deux points peut être expliquée par le fait que le modèle ne simule qu'une seule transition d'occupation du sol, la déforestation, qui récupère donc les quantités de changements estimées pour les autres transitions.

\input{Partie2/figures/tables/tab5}

Les comparaisons visuelles ne sont cependant pas suffisantes pour juger de la qualité des prédictions d'un modèle, c'est pourquoi de nombreuses méthodes de validation ont été mises au point, à la fois pour évaluer les sorties *hard* et les sorties *soft* des logiciels de modélisation des changements d'occupation et d'usage du sol. Ces techniques, recensées par @paegelow2018c sont nombreuses, diverses et complémentaires. Certaines évaluent les aspects quantitatifs de la prédiction alors que d'autres s'intéressent aux aspects qualitatifs, comme à la localisation des erreurs et des exactitudes dans la prédiction des changements.

En ce qui concerne les prédictions dites *soft* (figure~\ref{fig:fig7}) issues des modèles, la courbe ROC (*receiver operating characteristic curve*) demeure une des méthodes de validation les plus utilisées dans la littérature pour évaluer la qualité des *soft predictions* [@mas2018a ; @camachoolmedo2022] :  *"ROC evaluates the model until the change potential stage, but it is unable to assess the LUC simulated map which aims at mimicking temporal and spatial patterns.* [@mas2013]. Inventée pendant la Seconde Guerre mondiale pour distinguer les signaux radar du bruit de fond, cette technique est dédiée à la mesure de la performance d'un classificateur binaire. Portée dans le champ de la *land system science* par @pontius2001, la courbe ROC évalue la qualité de la prédiction des changements d'occupation et d'usage du sol en termes de localisation. Concrètement, il s'agit d'évaluer la proportion de vrais et de faux positifs à partir d'une carte de référence binaire — les changements réellement survenus entre les deux dates de référence — et d'une carte continue, comme celle du  potentiel de transition. L'interprétation se fait ensuite en comparant la courbe ROC à une droite théorique exprimant une distribution aléatoire  : plus l'indice AUC\\footnote{Pour *area under curve*, l'aire sous la courbe.} est élevé, plus la prédiction est jugée performante. Ainsi, par exemple, si la carte du potentiel de transition correspond parfaitement à celle de la déforestation survenue sur la période, AUC = 1.  À l'inverse, AUC = 0.5 en cas de distribution parfaitement aléatoire.

Les travaux de @pontius2000 relatifs à la différenciation des erreurs de prédiction dues à la quantité de celles dues à la localisation ont remis en cause l'utilisation du Kappa de Cohen\\footnote{Le coefficient K de Cohen est un test statistique non paramétrique qui permet d'évaluer la concordance entre deux classifications catégorielles d'un même objet.} jusque là usuellement utilisé pour évaluer la concordance entre une carte de l'occupation du sol de référence et une autre simulée [@pontius2008a ; @pontius2011]. En réponse à ces critiques, @chen2010 proposent une méthode de budgétisation des erreurs et des exactitudes des prédictions de changements à partir du croisement de trois cartes : les deux cartes de référence de la période de simulation (*t1* et *t2*) et la carte de prédiction (*t2\textunderscore sim*). À la suite de cette procédure, les pixels sont ainsi classifiés en quatre catégories : ceux où on observe une persistance de classe entre les deux dates et où le modèle simule une persistance (*Null Successes*) ; ceux pour lesquels une persistance est observée mais où le modèle prédit un changement (*False alarms*)~~;~~les pixels pour lesquels le changement qui se produit est prédit correctement par le modèle (*Hits*) et enfin ceux où un changement a été observé entre les deux dates mais n'a pas été prédit par le modèle (*Misses*).

Les deux sous-sections suivantes sont consacrées à l'étape d'évaluation/validation de notre modèle PBM. Les deux méthodes évoquées ci-dessus (courbe ROC et budgétisation des erreurs et exactitudes) sont mises en œuvre après un bref commentaire de métriques paysagères (*landscape metrics*) calculées sur les résultats de la simulation.


\subsection{*Landscape metrics*}
\label{subsec:LMpbm}

Afin d'évaluer la capacité du modèle à reproduire les *patterns* observés dans la "réalité" (classification MAE-MAGAP pour l'année 2016, soit l'image *t2*), nous avons calculé un ensemble de métriques paysagères sur la carte *hard prediction* issue de la simulation (*t2\textunderscore sim*). Dans le tableau~\ref{tab:LMPBM}, le résultat des calculs est comparé à ceux déjà effectués pour *t2*, présentés dans la section \ref{subsec:chap3-3-2-3} du chapitre 3. Seules les classes forêts (FOR) et agriculture (AGR), qui contiennent l'essentiel des dynamiques de déforestation, ont été conservées.

\input{Partie2/figures/tables/LM_PBM}

Les résultats montrent d'abord que, pour les deux classes, le paysage est moins fragmenté sur la carte simulée que sur la classification de l'image satellite (sous-estimation du nombre de patches NP et surestimation de leur superficie moyenne AREA MN). Cela peut être expliqué d'une part par le fonctionnement de Land Change Modeler [@eastman2018], qui simule uniquement les changements contigus à la déforestation existante et pas les changements isolés\\footnote{LCM possède un mécanisme d'*expander* mais pas de *patcher*, pour reprendre la terminologie utilisée par les concepteurs de Dinamica EGO [@rodrigues2018].} et d'autre part parce qu'aucun sous-modèle de reforestation n'est programmé. Le NDCA et l'ED sont sous-estimés pour la même raison. Le LPI et le CPLAND, qui indiquent respectivement la taille du plus gros patch de la classe et la part du territoire couverte par les zones centrales des patches de la classe sont des *patterns* mieux reproduits par la simulation, où on observe une différence de seulement quelques pourcentages et points de pourcentage par rapport à la réalité. Enfin, la distance euclidienne moyenne au voisin le plus proche (ENN MN) est surestimée par le modèle pour les deux classes, ce qui est très certainement là encore une conséquence de l'insuffisant morcellement du paysage par rapport à la réalité (une fragmentation aurait tendance à diminuer la distance moyenne entre patches de la même classe).


\subsection{Courbe ROC}
\label{subsec:chap4-2-3-1}

La courbe ROC (Figure~\ref{fig:fig9}) est calculée sous TerrSet afin d'évaluer la carte du potentiel de transition (Figure~\ref{fig:fig7}) à l'aune de la déforestation survenue entre 2008 et 2016. Le logiciel découpe et classe par ordre décroissant les valeurs du potentiel de transition selon 100 seuils d'intervalles égaux. L’occurrence de chaque classe est ensuite comparée à la carte binaire afin de départager les faux positifs des vrais positifs.

\begin{figure}[h!]\centering
\includegraphics[width=\linewidth]{Partie2/figures/roccurvelast2.png}
\caption[Courbe ROC]{Courbe ROC évaluant la carte du potentiel de transition.}\label{fig:fig9}
\end{figure}


Ici, l'aire sous la courbe (AUC) est d'environ 0.87, ce qui montre que notre modèle tient compte des valeurs du potentiel de transition afin d'allouer spatialement les quantités de changement prévues par le modèle. Attention cependant, l'AUC peut être tirée vers le haut par la persistance correctement prédite (*Null Successes*), à distinguer des changements correctement prédits (*Hits*, voir US \citealp[p.31]{usnrc2014}).


\subsection{Budgétisation des erreurs et des exactitudes}
\label{subsec:chap4-2-3-2}

La méthode de @chen2010 est mise en œuvre pour évaluer les résultats de la sortie *hard* de notre modèle. Rappelons qu'une "sortie *hard*" désigne une carte catégorielle où une classe d'occupation du sol est définitivement assignée à chaque pixel à l'issue de la procédure d'allocation spatiale MOLA, soit de la carte *t2\textunderscore sim* (figure~\ref{fig:fig8}).

\begin{figure}[p]\centering
\includegraphics[width=\linewidth]{Partie2/figures/chenpontius2.png}
\newline
\newline
\newline
\includegraphics[width=.7\linewidth]{Partie2/figures/plotzoom.png}
\caption[Erreurs et exactitudes de la prédiction : croisement pixel à pixel des deux cartes de références et de la carte simulée.]{Erreurs et exactitudes de la prédiction : croisement pixel à pixel des deux cartes de référence et de la carte simulée.}
\label{fig:fig10}
\end{figure}

La figure~\ref{fig:fig10} est le résultat du croisement pixel à pixel de trois cartes selon la méthode de [@chen2011] : les deux cartes de référence *t1* (2008) et *t2* (2016) puis la carte produite par le modèle à l'issue de la simulation pour l'année 2016, *t2\textunderscore sim*\\footnote{Pour "croiser" pixel à pixel ces trois cartes, la fonction *Crosstab* de Terrset est employée.}. On peut apercevoir sur la carte et sur le graphique qu'une grande partie de la persistance de la classe "forêt" observée est prédite par le modèle (*Null Successess*, plus de 91\% du territoire). Le modèle semble en revanche avoir plus de mal à prédire les changements puisque les *Hits* — qui désignent les changements observés et prédits par le modèle — ne concernent qu'un peu plus de 1\% du territoire d'étude, contre près de 3.6\% pour les changements observés et non prédits par le modèle (*Misses*) et 3.6\% pour les changements prédits mais non observés (*False alarms*).  À l'observation de la carte, on peut également affirmer que la plupart des *Misses* (en rouge) sont des îlots isolés, fragmentés et éloignés de la *vía Auca*, ce qui suppose des difficultés à simuler la déforestation non-contiguë à celle qui préexiste, c'est-à-dire l'apparition de nouvelles coupes. Ce constat est appuyé par l'observation des *False Alarms* (en orange), qui sont quant à elles plutôt regroupées et occupent en moyenne plus d'espace. Ceci n'a rien de surprenant, puisque LCM simule un mécanisme d'expansion des changements qui repose sur la mise en application concrète de la prédiction *soft* évoquée précédemment, qui elle-même repose sur le poids des facteurs de changement. Or, nous avons vu que la distance à la route et la distance à la déforestation existante constituaient des variables explicatives majeures au regard des changements passés.

Une budgétisation plus approfondie des changements peut être réalisée à partir de ces informations, notamment le calcul de la part d'erreur due à la quantité de changements prédits (Q), celui de la part d'erreur due à l'allocation spatiale des changements prédits (A) et donc celui de l'erreur totale (T). Le tableau~\ref{tab:tab6} présente les calculs à effectuer pour obtenir ces indices de budgétisation.

\input{Partie2/figures/tables/tab6}

Ici, l'erreur due à la quantité de changements prédits (Q) s'élève à seulement 0.1\%, contre 7\% d'erreur due à l'allocation spatiale des changements (A), pour une erreur totale (T) de 7.1\%. Ces données sont fort utiles à la validation de notre modèle et au commentaire de ses résultats, elles montrent que les erreurs sont quasi-entièrement dues à l'allocation spatiale des changements. 

D'autres pistes de validation pourraient être imaginées pour valider les résultats des modèles PBM. Pour mieux évaluer l'importance d'un taux d'erreur due à l'allocation spatiale, il pourrait par exemple être intéressant de quantifier la distance moyenne entre les pixels désignés *False Alarms* et les pixels *Misses*, afin de savoir à quel point certes, le modèle ne "tape pas dans le mille", mais s'en approche ou s'en éloigne. Les méthodes de validation mobilisant la logique floue [@mas2018b] pourraient également s'avérer plus pertinentes, afin notament de dépasser le cadre de la stricte concordance "pixel à pixel".

\section{Conclusion}
\label{sec:chap4-3}

Le modèle *pattern-based* (PBM) présenté ici avait pour objectif de reproduire les dynamiques de déforestation survenues entre les années 2008 et 2016 sur notre territoire d'étude, la *parroquia* de Dayuma (simulations rétrospectives). À partir de l'analyse des changements passés (2000-2008) et de leurs relations avec un ensemble de variables explicatives relatives aux contextes physique et socio-démographique, ce modèle dit *"business-as-usual"* \citep{mejean2020} simule la quantité et la localisation des changements pour une période équivalente à celle de la période de calibration, en "prolongeant" (ou "projetant") sa dynamique. Ce modèle parcimonieux illustre parfaitement le fonctionnement de la démarche de modélisation PBM et pourra ainsi servir notre réflexion sur les pratiques de modélisation et de simulation des changements d'occupation et d'usage du sol.

La transformation des données socio-démographiques issues du recensement de la population en variables spatiales explicatives (*drivers*) employées dans un modèle PBM constitue une certaine originalité. Ce procédé remplit en partie l'objectif d'amélioration de la prise en compte des processus sous-jacents par les approches *pattern-based* (évoqué à la fin du chapitre 2 --- cf.~section~\ref{subsec:chap2-3-7}). Les variables socio-démographiques spatialisées ne remplacent certes pas une modélisation dynamique des processus et mécanismes sous-jacents mais elles les représentent par procuration \footnote{Cette idée de représentation par procuration fait ici référence à la notion de "proxy", également utilisée en climatologie \citep{petit2020}.}. Plusieurs composantes de forces motrices de la déforestation en Amazonie équatorienne --- telles que le cycle de vie des ménages ou la migration --- ont de cette façon pu être "approchées" dans le modèle.

Malgré cela, LCM prédit difficilement la localisation des changements : l'analyse des résultats de la simulation montre que les erreurs dues à une mauvaise allocation spatiale des changements sont prédominantes. La bonne prédiction de la persistance de classe "forêt" pèse également beaucoup trop lourd dans les succès de prédiction du modèle. La déforestation correctement anticipée spatialement ne concerne en effet qu'un peu plus de 1\% du territoire. Le modèle prédit donc mal la localisation des nouveaux espaces déforestés et échoue également à reproduire la structure spatiale des changements. Alors qu'on observe que ceux-ci apparaissent plutôt en marge de la déforestation déjà existante, tel un mouchetage d'îlots distants, LCM prédit des changements majoritairement contigus.

Ces résultats de simulation mitigés sont à nuancer. D'abord, parce que LCM propose d'autres fonctionnalités susceptibles d'améliorer les résultats de la simulation, que nous n'avons pas employées. Par exemple, l'ajout de contraintes spatiales telles que les zones d'incitation/de restriction des changements. Cette fonctionnalité aurait à la fois permis d'augmenter le potentiel de transition de certains pixels (par exemple ceux constituant un plan cadastral) et de réduire celui d'autres (par exemple, celui des pixels constituant le parc national Yasuní et les *comunas*, où la déforestation est bien moins présente voire inexistante). Ensuite, comme nous l'avons dit, la simulation des transitions autres que celle de la classe "forêt" à la classe "agriculture" aurait probablement amélioré les résultats de la simulation. Par ailleurs, compte tenu de l'importance de cette variable dans les dynamiques de changements d'occupation et d'usage du sol, LCM rend possible l'utilisation d'un fichier de routes dynamique représentant l'évolution du réseau routier à chaque itération de la simulation. Si un tel fichier avait été en notre possession, les résultats de la simulation auraient été différents. Enfin, il est important de rappeler que l'évaluation des sorties des modèles PBM dépend de la qualité des classifications d'occupation du sol, définies comme "données observées" alors qu'il s'agit d'informations acquises par télédétection, une méthode qui n'est pas dénuée de limites et d'incertitudes.

D'une manière plus générale, ce modèle illustre également les limites et les reproches faits à la démarche de modélisation PBM dans la littérature. La première limite des modèles PBM est celle de la disponibilités des données : toutes les variables pouvant expliquer les changements d'occupation et d'usage du sol d'un territoire ne sont pas convertibles en variables spatialisées afin de pouvoir être utilisées lors de la calibration du modèle. Ceci restreint donc les choix de variables du modélisateur à quelques données somme toute assez générales (relief, distance à la route, etc.) ou bien peu précises (calcul de la répartition de la population par interpolation spatiale, comme nous l'avons fait ici). D'autre part, le nombre de variables pouvant être prises en compte pour l'entraînement d'un modèle est limité, ce qui ne correspond pas à la réalité d'un système complexe. Autre reproche abondamment évoqué dans la littérature, les modèles PBM reposent sur le postulat de la stationnarité (dans le temps et l'espace) des relations entre les variables explicatives et les changements, ce qui rend la prospective compliquée car ces relations peuvent être amenées à évoluer dans le temps en fonction d'éléments contextuels évidents. C'est d'ailleurs pourquoi la modélisation PBM est jugée peu efficace pour la prospective à long terme (US~\citealp{usnrc2014}), au-delà de cinq ans : les bifurcations et changements de trajectoire des systèmes socio-environnementaux ne pourraient être pris en compte. Permettre une variabilité temporelle dans la relation entre les *drivers* et les changements au cours de la simulation permettrait de pallier à cet écueil, à l'image de la fonctionnalité de variabilité spatiale permise par le logiciel Dinamica EGO [@rodrigues2018], qui propose un "entraînement zonal" pour les modèles PBM. De la même façon, Dinamica EGO propose un mécanisme de simulation des changements basé sur leur génération selon deux formes spatiales différentes : les changements dus à une expansion (via l'*expander*) des espaces déjà déforestés et ceux qui apparaissent de manière isolée (via le *patcher*). Ceci permet de reproduire avec plus de réalisme les dynamiques de fragmentation des espaces forestiers que l'on peut observer en contexte de front pionnier.

Cependant, les modèles PBM tel que celui que nous venons de présenter demeurent une approche *top-down*, qui n'explique pas les changements d'occupation du sol mais a pour objectif leur prédiction ainsi que la construction de scénarios tendanciels et/ou contrastés. L'ensemble des moyens et des outils mis en œuvre par les différentes plate-formes de modélisation existantes pour tenter d'améliorer les résultats des simulations (mécanisme de *patcher*/*expander*, contraintes et incitations, fichier de route dynamique, zoning, etc.) sont pertinents au regard de certaines attentes du modélisateur mais ne font que rendre les modèles moins généralisables, car de plus en plus modelés selon un contexte spécifique. L'ensemble de ces fonctionnalités ajoutées aux plate-formes de modélisation PBM nous paraissent tout au plus comme des greffons palliatifs à l'inconvénient majeur de cette approche de modélisation : elle ne formalise pas des processus. Néanmoins, d'une certaine façon, les processus se manifestent dans les modèles PBM à travers les taux d'erreur des simulations, qui révèlent les *drivers* manquants au modèle, reflets des processus qui n'ont pas été pris en compte.

Le chapitre suivant est dédié à la présentation de notre modèle *process-based* --- un modèle à base d'agent (ABM) --- qui a lui aussi pour objectif de simuler les dynamiques de déforestation de notre territoire d'étude. Ce modèle issu d'une approche qualifiée de *bottom-up* est quant à lui basé sur une représentation des processus sous-jacents.
